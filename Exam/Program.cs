﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Twilio;
using Twilio.Rest.Api.V2010.Account;

namespace Exam
{
    class Program
    {

        public static bool CheckPhone(UserContext userContext,string phoneNumber){
            bool anyUser = userContext.Users.Any(s => s.PhoneNumber == phoneNumber);
            return anyUser;
        }
        static void Main(string[] args)
        {
            UserContext userContext = new UserContext();
            while (true) {
                Console.Clear();
                Console.WriteLine("1 - Registration ");
                Console.WriteLine("2 - Recovery ");
                Console.WriteLine("3 - Exit ");
                string choose = Console.ReadLine();
                int chooseInt;
                bool success = int.TryParse(choose,out chooseInt);
                Random random = new Random();
                string current = random.Next(1000, 9999).ToString();

                if (success)
                {
                    switch (chooseInt) {
                        case 1:
                            Console.Clear();
                            Console.Write("Enter phone number: +7 ");
                            string phoneNumber = "7"+Console.ReadLine();
                            
                            long phoneNumberInt;
                            
                            bool successPhoneNumber = Int64.TryParse(phoneNumber,out phoneNumberInt);
                             if (successPhoneNumber) {
                                if (!CheckPhone(userContext, phoneNumber))
                                {

                                    const string accountSid = "AC849627a77162e82a62388508929a3497";
                                    const string authToken = "251555ec3e87e0f53709251d57acfd60";

                                    try
                                    {
                                        TwilioClient.Init(accountSid, authToken);

                                        var message = MessageResource.Create(
                                            body: "Verification code: " + current,
                                            from: new Twilio.Types.PhoneNumber("+1 859 657 8234"),
                                            to: new Twilio.Types.PhoneNumber("+" + phoneNumber)
                                        );

                                        //Console.WriteLine(message.Sid);


                                    }
                                    catch (Exception exception)
                                    {
                                        Console.WriteLine(exception.Message);
                                        Console.Read();
                                        break;

                                    }

                                    int countVerifyCode = 0;
                                    bool successVerifyCode = false;
                                    while (countVerifyCode <= 3 && successVerifyCode)
                                    {
                                        Console.Write("Enter verification code: ");
                                        string verifyCode = Console.ReadLine();

                                  
                                        if (verifyCode == current)
                                        {
                                            userContext.Users.Add(new User { PhoneNumber=phoneNumber, CodeNumber = verifyCode});
                                            successVerifyCode = true;
                                            userContext.SaveChanges();

                                        }
                                        else
                                        {
                                            Console.WriteLine("Incorrect code!");
                                        }
                                        countVerifyCode++;
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("Already verified!");
                                }
                                Console.Read();
                            }
                            else {
                                Console.WriteLine("Enter numbers!");
                                Console.Read();
                            }
                            break;
                        case 2:
                            Console.Clear();
                            Console.Write("Enter phone number: +7 ");
                            string phoneNumberUpdate = "7" + Console.ReadLine();

                            long phoneNumberIntUpdate;

                            bool successPhoneNumberUpdate = Int64.TryParse(phoneNumberUpdate, out phoneNumberIntUpdate);
                            if (successPhoneNumberUpdate)
                            {
                                if (CheckPhone(userContext, phoneNumberUpdate))
                                {

                                    const string accountSid = "ACfa1dc641f65b4553982e12d1ed28e783";
                                    const string authToken = "de34e33c7f9562be46da7dbe240ec7dc";

                                    try
                                    {
                                        TwilioClient.Init(accountSid, authToken);

                                        var message = MessageResource.Create(
                                            body: "Verification code: " + current,
                                            from: new Twilio.Types.PhoneNumber("+1 959 666 6446"),
                                            to: new Twilio.Types.PhoneNumber("+" + phoneNumberUpdate)
                                        );

                                        //Console.WriteLine(message.Sid);


                                    }
                                    catch (Exception exception)
                                    {
                                        Console.WriteLine(exception.Message);
                                        Console.Read();
                                        break;

                                    }

                                    int countVerifyCode = 0;
                                    bool successVerifyCode = false;
                                    while (countVerifyCode <= 3 && !successVerifyCode)
                                    {
                                        Console.Write("Enter verification code: ");
                                        string verifyCode = Console.ReadLine();


                                        if (verifyCode == current)
                                        {
                                            var rename = from i in userContext.Users
                                                         where i.PhoneNumber == phoneNumberUpdate
                                                         select i;
                                            foreach (var r in rename)
                                            {
                                                r.CodeNumber = verifyCode;
                                                
                                                // Insert any additional changes to column values.
                                            }
                                            userContext.SaveChanges();
                                            successVerifyCode = true;

                                        }
                                        else
                                        {
                                            Console.WriteLine("Incorrect code!");
                                        }
                                        countVerifyCode++;
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("Not registered!");
                                }
                                Console.Read();
                            }
                            else
                            {
                                Console.WriteLine("Enter numbers!");
                                Console.Read();
                            }
                            break;
                        case 3:Environment.Exit(0);
                            break;
                    }
                }
                else {
                    Console.WriteLine("Enter 1-3!");
                    Console.Read();
                }
            }
            
        }
    }
}
